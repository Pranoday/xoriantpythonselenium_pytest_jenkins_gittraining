#In class methods 1st parameter holds the reference of the object
#used to call the method
#We can mention any name to this parameter
import abc
from typing import Final, final

from multipledispatch import dispatch

from AccountSystemExceptions.InsufficientBalanceException import InsufficientBalanceException

class Account(abc.ABC):
    #Class variable
    _InterestRate:Final=10.0             #INterest_Rate is a CLASS VARIABLE with PROTECTED ACCESS
    #def __init__(self,ano,bal):
    def __init__(self, *AccountDetails):
        if(len(AccountDetails)==1):
            #Instance variables
            self.__AccountNo=AccountDetails[0]#Defining variable with __ makes variable Private
            self._Balance=0 #Defining variable with _ makes it Protected
        elif(len(AccountDetails)==2):
            self.__AccountNo = AccountDetails[0]
            self._Balance=AccountDetails[1]
        else:
            raise Exception("Please send only 2 arguments,1st for AccountNo and 2nd for Balance")

    @dispatch(int)
    def DepositeAmount(self,AmntToBeDeposited):
        self._Balance=self._Balance+AmntToBeDeposited
        _InterestRate=20.0
    @dispatch()
    def DepositeAmount(self):
        self._Balance = self._Balance + 1000


    @final
    def WithdrawAmount(self,AmountToBeWithdrawn):
        if self._Balance<AmountToBeWithdrawn:
            #raise is equivalent to throw in java
            raise InsufficientBalanceException()
        self._Balance=self._Balance-AmountToBeWithdrawn

    def GetBalance(self):
        return self._Balance
    def CloseAccount(self):
        print("Account closing may take time depending upon type of account")

    @abc.abstractmethod
    def OpenAccount(self):
        pass