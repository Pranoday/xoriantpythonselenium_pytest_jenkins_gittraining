#In python all member are public by default
from AccountSystemExceptions.InsufficientBalanceException import InsufficientBalanceException


class Account:
    _InterestRate=10.0      #This is a Protected Class Variable
   # def __init__(self,Ano,Bal):
    #    self.AccountNo=Ano
     #   self.Balance=Bal

    #def __int__(self,Ano):
     #   self.AccountNo=Ano
    def __init__(self, *Args):
        self.AccountNo=0
        self._Balance=0             #This is Protected Variable
        if len(Args) == 1:
            self.AccountNo=Args[0]
        else:
            self.AccountNo=Args[0]
            self.Balance=Args[1]

    def DepositeAmount(self,AmountToBeDeposited):
        self.Balance=self.Balance+AmountToBeDeposited
        return self.Balance

    def WithdrawAmount(self,AmountToBeWithdrawn):
        if(self.Balance<AmountToBeWithdrawn):
            raise InsufficientBalanceException

    def GetBalance(self):
        return self.Balance

    def EarnInterest(self):
        print("Implemented by Parent Class")
        EarnedInterest=(self.Balance*self.InterestRate)/100;
        self.Balance=self.Balance+EarnedInterest
