from AccountSystem.Account import Account
from AccountSystemExceptions.InsufficientBalanceException import InsufficientBalanceException
from TypesOfAccounts.SavingsAccount import SavingsAccount

Act=Account(1,1000)
print(Act.GetBalance())
Act.DepositeAmount(200)
print(Act.GetBalance())
try:

    Act.WithdrawAmount(5000)
except InsufficientBalanceException:
        print("You do not have sufficient balance in you account")


SAct=SavingsAccount(2)
#print(SAct.GetBalance())

SAct1=SavingsAccount(3,10000)
print(SAct1.GetBalance())

#print(SAct.InterestRate)
#print(SAct1.InterestRate)

SAct1.EarnInterest()