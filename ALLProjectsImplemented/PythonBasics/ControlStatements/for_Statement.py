#for loop
#In java for loop would be:{int =1;i<=10;i++}
Fruites=["Apple","Orange","Banana","Mango"]

#in for loop loop variable interates over collection present after in
for Fruite in Fruites:
    print(Fruite)

#Printing each character from String
Name="Pranoday"
for Character in Name:
    print(Character)

#Understanding for loop using range() function
for i in (range(6)):
    #for i in [1,2,3,4,5]
    print(i)

print("Customising range() function")
#We are asking range function to return us numbers from 2-10
for i in (range(2,11)):
    print(i)

print("Printing characters from name")
Name="Pranoday"
for i in (range(len(Name))):
    #range(8)
    print(Name[i])
    print(i)
print("Print name from 2nd character")
for i in range(1,4):
    print(Name[i])

#Print every 2nd number from Range
#range function returns numbers in order start of which can be mentioned as
#1 st argument,If we don't mention by default start is 0
#2nd argument to range function is where to stop.It is exclusive
#e.g. If I want to get numbers 1-10 range(1,11) should be used
#3rd argument defines the Increment,which is by default 1
#If I want to get EVERY SECOND number from 1-10,range(1,11,2) should be used
for i in range(0,11,2):
    print(i)

print("Print number is reverse order")
for i in range(10,0,-1):
    print(i)

print("Print name in reverse order")
Name="Pranoday"
#To print output in same line we have to send 2nd argument to print() as end=''
for i in range(len(Name)-1,-1,-1):
    print(Name[i],end='')


#Nested for Loops
print();
Lst1=["A","B","C"]
Lst2=[65,66,67]
for Char in Lst1:
    for Ascii in Lst2:
        #print(Char)
        #print(Ascii)
        print(Char,Ascii)