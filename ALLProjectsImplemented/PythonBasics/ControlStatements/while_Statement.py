#While statement
cnt=1
while(cnt<=10):
    cnt = cnt + 1;
    print(cnt)

#break statement
cnt=1
print("Break statement")
while(cnt<=50):
    if(cnt>10):
        break
    print(cnt)
    cnt=cnt+1
#else statement for while
print("Else for while statement")
cnt=1;

while(cnt<=5):
    i=1;
    print(cnt)
    cnt=cnt+1
else:
    #else gets executed after while condition gets false,at the time of coming out of the loop
    print(cnt)
    print(i)
print(cnt)
