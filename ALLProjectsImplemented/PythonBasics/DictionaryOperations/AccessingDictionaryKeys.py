#Accessing Dictionary elements
Car={"Name":"Celerio","Make":"MarutiSuzuki","IsMadeInIndia":True,"Price":500000.50}
print(Car["Name"])

#Printing ALL KEYS from Dictionary
print("Following are the keys exist in Dictionary: ")
for Key in Car.keys():
    print(Key)

#Printing All VALUES from Dictionary
print("Following are the values in Dictionary")
for Key in Car.keys():
    print(Car[Key])

#Printing Key and its corresponding value
print("Printing Key and Value pair")
for K in Car.keys():
    print(K,Car[K])

#Printing Collection of Values
print(Car.values())
print(type(Car.values()))
print("Printing Key and Value as Tuple")
for Item in Car.items():
    print(Item)

#Checking for the existence of a Key
#in operator if used on Car.Keys returns true if pareticular Key is present,false otherwise
if("IsMadeInIndia" in Car.keys()):
    print(Car["IsMadeInIndia"])

#In operator can also be used to check existence in Values as well
if(False in Car.values()):
    print("Value Present")
else:
    print("Value is NOT Present")

