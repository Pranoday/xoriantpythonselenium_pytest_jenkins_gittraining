#Arbitrary keyword parameter should have ** prior to it
#Arbitrary parameter should have * prior to it

#Arbitrary parameter values would accepted as Tuple
#Arbitrary Keyword parameter values would accepted as Dictionary
def PrintPersonDetails(**Kwords):
    print(Kwords)
    print(Kwords["FirstName"])
    print(Kwords["LastName"])
    print(Kwords["Marks"])

    for K in Kwords:
        print(K,Kwords[K])

PrintPersonDetails(FirstName="Pranoday",LastName="Dingare",Marks=100.00)