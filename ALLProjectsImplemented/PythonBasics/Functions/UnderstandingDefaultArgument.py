#Default parameters can be mentioned in defination of function
#If we have multiple parameters for a function then
#REQUIRED parameters should be mentioned FIRST followed by OPTIONAL parameters
#Function definition does not contain any RETURN TYPE
#We can return only 1 value from function
def Contcatenation(Str2,Str1="Pranoday"):
    return Str1+Str2;
#We can return List/Dictionary/tuples as well from function
def ListOfNumbers():
    lst=[10,20,"PD",True,100.00]
    return lst

print(Contcatenation("Dingare"))
print(ListOfNumbers())
