#def Addition(a,b):
 #   return a+b

#print(Addition(100,200))
#Arbitrary parameters can be defined using *
#Function having arbitrary argument can be called with any number of arguments
#These sent arguments get recieved as tuple
def Addition(*Args):
    sum=0
    for num in Args:
        sum=sum+num;
    return sum;

def Subtraction(a,b):
    return a-b

print(Addition(10,20,30,40))
print(Subtraction("Pranoday","Dingare"))