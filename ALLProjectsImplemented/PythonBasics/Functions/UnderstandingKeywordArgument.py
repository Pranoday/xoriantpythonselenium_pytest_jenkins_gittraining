#Keyword arguments are where,while calling a function we mention
#parametername=parametervalue
#In this case we need to follow the order of parameters while sending in arguments

def my_function(child3, child2, child1):
  print("The age of child1 is " + str(child1))
  print("The age of child2 is " + str(child2))
  print("The age of child3 is " + str(child3))


my_function(child1=10,child3=4,child2=30)
my_function(10,30,4)

