
nums = [10, 20, 30, 40, 50, 60, 70, 80, 90]
#10 20 30   40  50  60  70  80  90
#0  1   2   3   4   5   6   7   8

#10 20  30  40  50  60  70  80  90
#-9 -8  -7  -6  -5  -4  -3  -2  -1

#Slicing from End
print("Printing last 3 elements from list")
print(nums[-3:])


#Mixing Positive and Negative numbers in Slicing
print("Printing using Postive and Negative numbers in Slicing notation")
#Original List:[10, 20, 30, 40, 50, 60, 70, 80, 90]
#O/P:[20, 30, 40, 50, 60, 70, 80]
print(nums[1:-1])
#Using Negative number in Start and Positive number in Stop
print("Printing using Negative in start and Positive in stop")
#Original List:[10, 20, 30, 40, 50, 60, 70, 80, 90]
#O/P:[70, 80]
print(nums[-3:8])

#Original List:[10, 20, 30, 40, 50, 60, 70, 80, 90]
#O/P:[50, 60, 70, 80]
print("Using Negative number in start as well as stop")
print(nums[-5:-1])


#10 20 30   40  50  60  70  80  90
#0  1   2   3   4   5   6   7   8

#10 20  30  40  50  60  70  80  90
#-9 -8  -7  -6  -5  -4  -3  -2  -1

#Reversing Lists
nums = [10, 20, 30, 40, 50, 60, 70, 80, 90]
print("Printing reverse list")
print(nums[::-1])
#Mentioning Negative step will access the elements from Right - Left
print("Printing using Negative Step")
print(nums[-2::-1])
print("Printing using Positive Step")
#Postive step will access elements from Left-Right
print(nums[-2::1])
#Orginal List:[10, 20, 30, 40, 50, 60, 70, 80, 90]
#O/P:[80, 70, 60, 50, 40, 30]
print("Slicing from Right-Left")
print(nums[-2:1:-1])
#Count of Stop is different when Step is Positive and when it is Negative
#If step is Positive and Stop is mentioned as 4,it would stop at 3(1 less from Left)
#If step is Negative and Stop is mentioned as 4,it would stop at 5(1 less from Right)

