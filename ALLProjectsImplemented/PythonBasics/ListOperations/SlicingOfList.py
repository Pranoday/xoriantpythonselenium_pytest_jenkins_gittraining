#Slicing of List
nums = [10, 20, 30, 40, 50, 60, 70, 80, 90]
#Retrieving 1st 3 elements from List
#List slicing has 3 parameters Start:Stop:Increment
#Stop argument is exclusive:It is by default len
#Start argument is inclusive :it is by default 0
#Increment is by default 1
#range(1,11)
print(nums[0])
#nums[0:3] will return us elements starting from 0th index till index 2
print("Printing 1st 3 elements")
print(nums[0:3])
print(nums[:3])

print("Printing In between elements from Index 3 to Index 6")
print(nums)
print(nums[3:7])

#If we ommit Stop parameter it takes len(list) as default
print("Printing whole list starting from given Index")
print(nums)
print(nums[3:])

#Printing whole list
#Printing whole list is possible by ommiting both Start and Stop parameters
print("Printing whole list is possible by ommiting both Start and Stop parameters")
print(nums[:])
#Printing every 2nd element from List
print("Printing whole list is possible by ommiting both Start and Stop parameters")
print(nums[::2])
print("Printing every 2nd element starrting from Index:1 to Index:4")
print(nums[1:5:2])


