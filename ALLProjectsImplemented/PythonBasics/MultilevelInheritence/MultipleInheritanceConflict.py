class A:
    def __init__(self):
        self.name = 'John'
        self.age = 23

    def getName(self):
        print("I am in A")
        return self.name


class B:
    def __init__(self):
        self.name = 'Richard'
        self.id = '32'

    def getName(self):
        print("I am in B")
        return self.name


class C(B, A):
    def __init__(self):
        B.__init__(self)
        A.__init__(self)

    #def getName(self):
     #   print("I am in C")
      #  return self.name


C1 = C()
print(C1.getName())
print(C1.name)
#Method Resolution Order can be printed.
print(C.__mro__)