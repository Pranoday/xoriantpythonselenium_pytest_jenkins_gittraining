#In java we have instance variables and stativ variables
#In Python we have instance variables and class variables

#In java instance as well as static variables are defined inside class body
#where static variables are defined using static keword

#In python variables defined inside class are class variables
#variables defined using self,inside constructor or methods,are instance vars

#In java constructors hold same name as that of class
#In python contructor is defined using __init__(self)

#self from python is equivalent to this in java
from typing import Final, final


class Account:
    _INTEREST_RATE:Final=10.0
    def __init__(self,*ActDetails):
        if(len(ActDetails)==1):
            self.AccountNo=ActDetails[0]
            self.Balance=0
        elif(len(ActDetails)==2):
            self.AccountNo = ActDetails[0]
            self.Balance=ActDetails[1]
        else:
            raise Exception("Please send 1 arguments,1st for AcctountNo and 2nd for Balance")

    def _DisplayDetails(self):
        print("Display details")

    def __CalculateAccountNo(self):
        pass
    @final
    def Meth(self):
        pass
    def EarnInterest(self):
        pass