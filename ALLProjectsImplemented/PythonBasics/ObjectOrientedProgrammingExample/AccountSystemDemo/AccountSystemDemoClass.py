#In java for creating an object syntax is:
#ClassName as a variable type objectname then new keyword and call to constructor
#In Python:
from ObjectOrientedProgrammingExample.AccountSystemClasses.AccountClass import Account
from ObjectOrientedProgrammingExample.TypesOfAccounts.SavingsAccountClass import SavingsAccount

Act1=Account(1,2)
print(Act1.Balance)

Sact1=SavingsAccount(1,2)
Sact1.DisplayAccountDetails()
Sact1.Meth()

Sact1.EarnInterest()