#Set are unordered collections
#Curly braces are used to define sets.
#Difference between Dictionary and Set:Even if { are used to define both
#but there is difference that Dictionary has Key-Value pair and Set has only Value
myset = {"apple", "banana", "cherry"}
print(myset)

print(type(myset))

for SetItem in myset:
    print(SetItem)

#Set does not allow duplicate values
print("Printing Set having duplicate values")
myset1={"apple", "banana", "cherry","apple"}
print(myset1)

#Length of myset1 will be 3 and NOT 4 as 1 element is present twice and Set does not
#allow duplicate values
print("Printing Length of Set")
print(len(myset1))

#A single set can have elements with multiple data types
SetOfMultipleDataTypes={True,"Pranoday",10,100.00}
print(SetOfMultipleDataTypes)

#Checking existence of an element in Set
print("Checking for existence of an element in Set")
print("PranodayD" in SetOfMultipleDataTypes)

#In set we can not modify existing element but we can add new elements
#In this was Set is different than touples

SetOfMultipleDataTypes.add("Katraj")
print(SetOfMultipleDataTypes)

thisset = {"apple", "banana", "cherry"}
tropical = {"pineapple", "mango", "papaya"}

#Update operation on Set will add the elements of another Set into original
print("Printing Original Set")
print(thisset)
thisset.update(tropical)
print("Printing updated Set")
print(thisset)

thisset = {"apple", "banana", "cherry"}
tropical = {"pineapple", "mango", "papaya","apple"}

print("Updating Set with another having duplicate elements")
thisset.update(tropical)
print(thisset)

print("Printing set after removing existing element")
thisset.remove("apple")
print("Printing set after removing existing element")
print(thisset)


thisset.discard("banana")
print("Printing set after discarding element")
print(thisset)

##Difference between remove and discard is:
##remove gives error if we try to remove no-existing element
##discard does not give error if we try to remove non-existing element
thisset.discard("Pranoday")
print("Printing set after discarding non-existing element")
print(thisset)


#Removing non-existing element from Set
##Removing non-existing element from Set gives error
##print("Printing set after removing non existing element")
##thisset.remove("Pranoday")
##print("After removing non existing element")
print("Original Set ",thisset)
print("Printing set after using pop")
thisset.pop()
print(thisset)

#Union will return us 3rd Set having All elements from 2 sets we are doing Union on
#Union function does not change existing set.It return new Set having elements
#from both the set we are performing union operation on.
set1 = {"a", "b" , "c"}
set2 = {1, 2, 3}


set3=set1.union(set2)
print("Printing set after union ",set3)

thisset.clear()
print("Printing set after clearing: ",thisset)
#del will remove the Set variable and will give error if we try to use it
del thisset
print(thisset)