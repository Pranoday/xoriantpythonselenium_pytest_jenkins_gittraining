age = 36
txt ="My name is John and my age is :"
#"My name is John, and I am {}"
print(txt+str(age))

age = 36
txt = "My name is John, and I am {}"
print(txt.format(age))

age=36
Experience=10
txt="My name is John, and I am {} years old and have experience of {} years"
print(txt.format(age,Experience))

#We can replace the placeholders by mentioning Index of arguments
#Index starts from 0
quantity = 3
itemno = 567
price = 49.95
myorder = "I want to pay {2} dollars for {0} pieces of item {1}."
print(myorder.format(quantity, itemno, price))
#If we don't mention Indexes in placeholders then
#Placeholders from left to right will be replaced by values of
#arguments in the order in which arguments are sent
myorder = "I want to pay {} dollars for {} pieces of item {}."
print(myorder.format(quantity, itemno, price))

#Split
Str="Hello World"
print("Splitting ",Str.split(" "))

Str=" Hello World "
print(Str.split("llo"))

#strip is like trim which removes spaces from Left and Right
print(Str.strip())
Str=" Hello World "
#lstring removes white spaces from left side of string like ltrim
print(Str.lstrip())

#rstrip() removes spaces from Right hand side like Rtrim()
Str=" Hello World "
print(Str.rstrip())

print(Str.replace("Hello","Hi"))