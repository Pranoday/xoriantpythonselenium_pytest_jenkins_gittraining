#If we are using functions/variables from different python file,
#then we have to import them in the current python file/module
#from <ModuleName> import <function/variable name>
#We can import particular function/variable from module using import statement
#If we want to import all functions/variables we have to use * in import statement
#from <modulename> import <members> should be used when we want to import
#specific member
#If we want to import whole module we have to use import<ModuleName>

