#Dictionary saves data in a Key-value pair.Dictionary is defined using Curly braces
#: is used to separate Key and Value
#Key and value both can be of any type,if Key and value are of String type mention them in " or '
#For accessing values from Dictionary we need to use Keys
Person={"Name":"Pranoday","Marks":100,14:"Experience"}
#To print value,if key is of type Str,we need to use syntax:Variable["KeyName"]
print(Person["Name"])
#If key is of type number Syntax:Variable[KeyName]
print(Person[14])
#Dictionary can be printed
print(Person)

#We can traverse each key and check for the type of each key using type() function
for key in Person.keys():
    print(type(key))
#Particular element can be changed with Syntax:VariableName[KeyName]=NewValue
Person["Name"]="Parag"
print(Person)

Dict={"Name":"Pranoday","Name":"Parag"}

print(Dict)

#type() function called using dictionary then O/P is class 'dict'
print(type(Dict))