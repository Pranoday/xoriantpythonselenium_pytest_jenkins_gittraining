#Lists in python are defined with Square Brackets i.e. []
Fruites=["Mango" , "Banana","Apple"]
print(Fruites)
#In python list can contain different types of Elements
Person=["Sushil",30,10000.00]
print(Person)

#List elements can be accessed using index,index starts with 0,Lists are ordered collection
print(Fruites[0])

print(Person[2])

#List elements can be updated by assigning new value
Fruites[1]="Orange"
print("Printing Fruites list after updating 1 item from it")
print(Fruites)

Fruites.append("Cherry")
#append operation appends new element at the end of the list.But it does not return anything
#print(Fruites.append("Cherry"))
print(Fruites)

#Duplicate elements are allowed in List
Names=["Pranoday","Dingare","Pranoday"]
print(Names)
#len() is a function return number of elements we have in List
print("Length of Fruites list after appending 1 more element is "+str(len(Fruites)))

#Calling type() function on List will return O/P:class<list>
print(type(Fruites))