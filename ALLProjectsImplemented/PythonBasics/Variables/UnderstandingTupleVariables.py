#Tuples are created using round brackets i.e. ()
#Tuples can not be modified
Fruites=("Apple","Orange","Banana")
Fruites1=()
Price=[100,200,300]
print(Fruites)
#Index in touple starts with 0,touple items can be accessed using Indexes
print(Fruites[0])

#Changing touple elements is not allowed.Following statement will give error
#Fruites[0]="Mango"
#print(Fruites)

print(len(Fruites))
Fruites1=Fruites
print(Fruites1)
