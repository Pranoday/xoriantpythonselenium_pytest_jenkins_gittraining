from selenium import webdriver
import time
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://material.angular.io/components/select/overview")
Dropdown=driver.find_element_by_id("mat-select-0")
Dropdown.click()
driver.find_element_by_xpath("//span[text()=' Pizza ']").click()

DivElement=driver.find_element_by_id("mat-select-value-1")
SelectedOptionText=DivElement.find_element_by_tag_name("span").text
print(DivElement.find_element_by_tag_name("span").text)
if(SelectedOptionText=="Pizza"):
    print("Correct option is selected..PASSED")
else:
    print("Correct option is not selected..FAILED")