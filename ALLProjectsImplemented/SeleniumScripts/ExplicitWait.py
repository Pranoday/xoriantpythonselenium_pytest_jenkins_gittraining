from selenium import webdriver
import time
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://opensource-demo.orangehrmlive.com/")
driver.find_element_by_id("txtUsername").send_keys("Admin")
driver.find_element_by_id("txtPassword").send_keys("admin123")

#Explicit wait is applicable for a particular element
#Un this example Driver will wait for 10 secs or Element becomes clickable whichever is earlier
wt = WebDriverWait(driver, 10)
wt.until (expected_conditions.element_to_be_clickable ((By.ID, "btnLogin")))
#expected_conditions.

driver.find_element_by_id("btnLogin").click()

