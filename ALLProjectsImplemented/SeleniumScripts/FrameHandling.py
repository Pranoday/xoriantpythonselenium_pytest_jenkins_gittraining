from selenium import webdriver
import time
driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
#Implicit wait.It would be applicable  for all the elements
#driver will wait for MAX 3 seconds for every element to appear in DOM
#This lasts untill the end of the test
#If element e.g. appeared in 1 sec.then test will move on to the next step
driver.implicitly_wait(20)
driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_test")
#Driver bydefault finds the elements inside MAIN html page
#Here we will get NoSuchElementException as element we are finding is not on MAIN page
#So we need to switch driver;s working context to the frame hosting the nested html
#page having required UI elements

#We can use ID/Name attribute of Iframe to swith driver's working context inside frame
driver.switch_to.frame("iframeResult")

driver.find_element_by_id("fname").send_keys("Pranoday")
FirstNameVal=driver.find_element_by_id("fname").get_attribute("value")

driver.find_element_by_name("lname").send_keys("Dingare")
LastNameVal=driver.find_element_by_name("lname").get_attribute("value")

driver.find_element_by_css_selector("input[type='submit']").click()
#time.sleep(5)
#If element has multiple CLASSES or CSS applied with space i.e. ' ' in between
#i.e. Composite class value is set to class attribute then
#use CSS selector and not find_element_by_class_name()

DivElement=driver.find_element_by_css_selector("div[class='w3-container w3-large w3-border']")

#We are retrieving element's contained text using text attribute
#There is no getText() function in Selenium python like java
DivElementText=DivElement.text
#In python logical and operator is and and in java it is &&
if(FirstNameVal in DivElementText and LastNameVal in DivElementText):
    print("Correct data is submitted..PASSED")
else:
    print("Correct data is NOT submitted..FAILED")
#While being inside nested page,driver will try to find run  button inside
#nested HTML page.But our Run button is inside MAIN HTML page
#Hence before we find run button we have to switch driver's working context back to
#MAIN page
#switch_to.default_content() will switch driver's working context to MAIN page
driver.switch_to.default_content()
#If element has multiple CLASSES or CSS applied with space i.e. ' ' in between
#i.e. Composite class value is set to class attribute then
#use CSS selector and not find_element_by_class_name()
#Here finding Run button
driver.find_element_by_css_selector("button[class='w3-button w3-bar-item w3-green w3-hover-white w3-hover-text-green']").click()
#driver.quit()