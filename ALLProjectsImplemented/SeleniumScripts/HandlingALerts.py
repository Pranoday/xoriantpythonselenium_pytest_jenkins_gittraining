from selenium import webdriver
import time
driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

NameField=driver.find_element_by_class_name("inputs")
#send_keys is for typing operation
NameField.send_keys("Pranoday")
time.sleep(5)
#Value typed inside input element of type TextBox,gets set to value attribute of DOM element
#We can retrieve the value of any attribute by specifying attribute name in
#get_attribute()
NameEntered=NameField.get_attribute("value")

#Syntax for CSS selectors query:TagName[AttributeName='AttributeValue']
#Using Cssselector as we are using value attribute for finding element
#Selenium does not have corresponding find_element API
#We can use Xpath as well but CSSselector should be preferred
AlertButton=driver.find_element_by_css_selector("input[value='Alert']")
AlertButton.click()
time.sleep(5)
#We are asking driver to leave DOM and switch working context to alert
MyAlert=driver.switch_to.alert
#Retrieving text of alert
AlertMessage=MyAlert.text
if(NameEntered in AlertMessage):
    print("Correct alert message is shown")
else:
    print("Correct alert message is NOT shown")
#We are closing alert by accepting
MyAlert.accept()
time.sleep(5)
#Handling Confirmation popup
NameField.send_keys("Pranoday")
NameEntered=NameField.get_attribute("value")
time.sleep(5)
driver.find_element_by_id("confirmbtn").click()

MyAlert=driver.switch_to.alert
#Retrieving text of alert

AlertMessage=MyAlert.text
if(NameEntered in AlertMessage):
    print("Correct message is shown in Confirmation alert")
else:
    print("Correct message is NOT shown in Confirmation alert")
MyAlert.dismiss()