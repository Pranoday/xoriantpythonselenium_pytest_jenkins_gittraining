from selenium import webdriver
import time
driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

#Finding Benz checkbox using id attribute
BenzCheckbox=driver.find_element_by_id("benzcheck")
#Clicking on Benz checkbox
BenzCheckbox.click()
time.sleep(3)
#is_selected() returns boolean True if element is selected/checked otherwise returns boolean False
IsChecked=BenzCheckbox.is_selected()
if(IsChecked==True):
    print("Clicking on Checkbox makes it Checked..PASSED")
else:
    print("Clicking on Checkbox does not make it Checked..FAILED")
#Here clicking on Benz checkbox to make it Unchecked
BenzCheckbox.click()
#is_selected() should return boolean False
time.sleep(3)
IsChecked=BenzCheckbox.is_selected()
if(IsChecked==False):
    print("Clicking on Checked Checkbox makes it Unchecked..PASSED")
else:
    print("Clicking on Checked Checkbox does not make it Unchecked..FAILED")
