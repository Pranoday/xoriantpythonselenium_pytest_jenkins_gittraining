from selenium import webdriver
import time
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

driver.find_element_by_id("openwindow").click()
print(driver.current_window_handle)

#We will have Handles of both windows in Handles variable
#Here we get AllWindow handles in List
#In java we get them in Set
Handles=driver.window_handles

for Handle in Handles:
    print(Handle)
    driver.switch_to.window(Handle)
    if(driver.title=='Let\'s Kode It'):
        break;
driver.find_element_by_link_text("Login").click()
driver.switch_to.window(Handles[0])
print("Handle of original window is : "+driver.title)
#Closes the current windows
#driver.close closes CURRENT WINDOW:CURRENT WINDOW is window handle of which
#driver is possessing.
driver.close()
#Terminating driver.exe and terminating the script session
driver.quit()



