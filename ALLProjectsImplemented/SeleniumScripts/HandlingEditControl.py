#We need to start BrowserDriver and establish communication session with it
#Every selenium API will be sent to browserdriver and it would be translated into
#browser native UI automation APIs and browsers would be automated

from selenium import webdriver
import time
#In java:
#System.setProperty("webdriver.chrome.driver","PATH of Driver")
#ChromeDriver driver=new ChromeDriver()

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

#Click on Hide button
#Check if textbox is hidden or not
#Click on Show button
#Check textbox gets shown back

#Image Identification:Identifying  elements using Image of control
#Native Identifiication:Identifying elements using attributes of elements
#Relational Identification:If we are using relationship between elements then
#we re doing relational identification.We should use Xpath Axes for this

#Here we are identifying element using Id attribute
HideButton=driver.find_element_by_id("hide-textbox")
#We are clicking on button using click API
HideButton.click()
time.sleep(5)
TextBox=driver.find_element_by_name("show-hide")
#Is displayed returns boolean True if element is visible else it returns boolean False
IsVisible=TextBox.is_displayed()
if IsVisible==False:
    print("Hide button worked fine...PASSED")
else:
    print("Hide button did not work fine...FAILED")

#Identifying Sshow button
ShowButton=driver.find_element_by_id("show-textbox")
#Click on show button
ShowButton.click()
time.sleep(5)
#Retrieving visibility status of TextBox,which should return us boolean True
IsVisible=TextBox.is_displayed()

if IsVisible==True:
    print("Show button worked fine...PASSED")
else:
    print("Show button did not work fine...FAILED")




