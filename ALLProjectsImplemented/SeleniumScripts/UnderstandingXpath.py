#Xpath is a query language like SQL
#Xpath helps in extracting data from XML documents
#Xpath has got nothing to do with ONLY Element identification
#In SQL we write PREDICATES using where clause
#In Xpath we write PREDICATES using []
#select * from student where RollNo=1


#//table[@id='resultTable']/descendant::a[text()='Lisa']/parent::td/preceding-sibling::td[2]/child::input
from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")
PriceElement=driver.find_element_by_xpath("//table[@id='product']/descendant::td[text()='Python Programming Language']/following-sibling::td")
print(PriceElement.text)


PriceOfLastCourse=driver.find_element_by_xpath("//table[@id='product']/descendant::tr[last()]/child::td[last()]").text
print(PriceOfLastCourse)