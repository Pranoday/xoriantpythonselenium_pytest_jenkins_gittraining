#For pytest framework,to consider any python fils as test file,
# filename should be test_*.py OR *_test.py

#functions having names:test* i.e.functions names of which starts with test word
#are considered to be testcase functions and will be called by pytest

#Function name staring with word test is equivalent to mentioning @Test annotation
#from testNG in case of java

#If we want fixture code to be executed before testcase method
#then we have to mention fixture function name inside () after the name of TestCase Function
#In TestNG @BeforeMethod annotated method gets executed before every @Test method by default\
#In pytest we can choose which testcase function a fixture to be applied
import pytest

from CalculatorAPI.Calculator import Calculator
#Calc variable defined outside the functions is a Global variable
Calc=None

@pytest.fixture()
def CreateObj():
    #Even if we have a global variable with name Calc,here new local variable
    #with the name Calc will be created.So this local Calc and global Calc are different
    #Calc=Calculator()
    #We are mentioning to use Global variable Calc and assign it the object of
    #Calculator class
    #global Calc is ONLY clarifying to use already defined global variable
    #global keyword does not define global variables,when variable is defined outside
    #functions,it means it is indeed in global scope.
    #global keyword just asks to use variable from global scope instead of creating
    #new variable in local scope
    print("Fixture is creating an Object")
    global Calc
    Calc=Calculator()

@pytest.mark.SanityTest
def testAdditionWithPositiveNumbers(CreateObj):
    #Calc= Calculator()
    Res=Calc.Addition(30,50)
    assert Res==80
@pytest.mark.SanityTest
def testAdditionWithNegativeNumbers(CreateObj):
    #Calc = Calculator()
    Res = Calc.Addition(-1, -2)
    assert Res == -3
@pytest.mark.RegressionTest
def testAdditionWithZeroNumbers(CreateObj):
    #Calc = Calculator()
    Res = Calc.Addition(0, 0)
    assert Res == 1

def testAdditionWithZeroAndNonZeroNumbers(CreateObj):
    #Calc = Calculator()
    Res = Calc.Addition(0, 100)
    assert Res == 100