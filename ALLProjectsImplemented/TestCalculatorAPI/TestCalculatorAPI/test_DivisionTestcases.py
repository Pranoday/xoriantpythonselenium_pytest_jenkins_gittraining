import pytest

from CalculatorAPI.Calculator import Calculator


@pytest.mark.parametrize("num1, num2,output",[(10,2,5),(20,10,2),(30,3,10)])
def testDivisionWithPositiveValues(num1,num2,output):
    Cal=Calculator()
    Res=Cal.Division(num1,num2)
    assert Res==output

def testDivision(PositiveNumbers):
    Cal = Calculator()
    Res = Cal.Division(PositiveNumbers[0], PositiveNumbers[1])
    assert Res == PositiveNumbers[2]

