#In tnis file we can write COMMON/reusable fixtures which we would like to use
#across multiple testcase files

import pytest

from AutomationFramework.Utilities.Utils import Utils
from OrangeHRMDemoApp_Pages.LoginPage import LoginPage

#@pytest.fixture(params=[{"UserName":"Admin","Password":"","Error":"User name can not be empty"}])
@pytest.fixture(params=Utils.ReadLoginTestData())
def LoginDataProvider(request):
    #request.param returns item of list with each iteration
    return request.param

#Mentioning scope of fixture makes it work like BeforeClass from TestNG
@pytest.fixture(scope="class")
def CreateLoginPageObj(request):
    loginpage=LoginPage("Chrome")
    request.cls.loginpage=loginpage
    #We can get the reference of class requesting this fixture using request.cls
    #loginpage object we want to be passed to loginpage class variable of testcase class
    #request.cls.loginpage=loginpage will set loginpage variable from Testcase class
    #to object of loginpage PAGECLASS
    #return loginpage