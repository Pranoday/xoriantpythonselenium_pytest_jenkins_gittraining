#In java defining Data type of variable is mandatory
#int a=10
#Defining data types is not required in Python
a=10
#String literals can be mentioned either by using Double quotes(") or SingleQuotes(')
#type() is a function from python which return type of variable e.g. int,float,str etc.
print(a)
print(type(a))
a='Pranoday'
print(a)
print(type(a))
a=100.10
print(a)
print(type(a))


FirstName="Pranoday"
LastName="Dingare"
# + is the contcatenation operator
print(FirstName+" "+LastName)

YearsOfExperience=14
#print(FirstName+" has "+YearsOfExperience+" of experience")

#In Python we have to convert number in Strings using str() function before contcatenation
print(FirstName+" has "+str(YearsOfExperience)+" of experience")

#+ can also be used as an arithmetic operator
num1=10
num2=20
num3=num1+num2
print("Addition is "+str(num3))

