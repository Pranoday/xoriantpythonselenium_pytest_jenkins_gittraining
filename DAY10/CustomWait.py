from selenium.webdriver.chrome import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

#We can implement Custom waits by defining a class and
#implementing a __call__() method
#This method is supposed to return no False argument(either True/some WebElement e.g.)
# to be considered as met
#Using Custom wait we can impelement our own Conditions than available in ExpectedConditions class
class element_IsVisible(object):
  """An expectation for checking that an element has a particular css class.

  locator - used to find the element
  returns the WebElement once it has the particular css class
  """
  #Style attribute is set to display:block; while making Textbox visible
  def __init__(self, locator, StyleAttr):
    self.locator = locator
    self.css_class = StyleAttr

  def __call__(self, driver):
    element = driver.find_element(*self.locator)   # Finding the referenced element
    if self.css_class in element.get_attribute("style"):
        return True
    else:
        return False

#shortWait = new WebDriverWait(driver, 1, 500);


driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

wait = WebDriverWait(driver, 10)
element = wait.until(element_IsVisible((By.ID, 'myNewInput'), "myCSSClass"))

