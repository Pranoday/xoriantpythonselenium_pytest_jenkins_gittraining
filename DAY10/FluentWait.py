from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://opensource-demo.orangehrmlive.com/")
#Fluent wait is Explicit wait with SOME EXTRA CONTROL
#By default if condition does not meet while checked for the 1st time
#driver will wait for 500 Miliseconds before it will go again to check the
#condition.This time interval between 2 checks is called Polling time.
#In fluent wait we can configure POLLING TIME as well as we can configure driver
#to ignore exceptions which may occur during this wait time.

#In Selenium Python there is no separate FluentWait class available
# Load some webpage
wait = WebDriverWait(driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
element = wait.until(EC.element_to_be_clickable((By.XPATH, "//div")))

