import pytest

from CalculatorAPI.Calculator import Calculator

#If we want to run all testcases from particular testcase file
#Command:pytest <path of the Testcase file>

#If we want to run particular testcase function
#Then command:pytest -k <Word which is present in the name of testcase function we want to run>
@pytest.mark.SanityTest
def testMultiplicationWithPositiveNumbers():
    Calc = Calculator()
    Res = Calc.Multiplication(10,10)
    assert Res==100

def testMultiplicationWithZero():
    Calc = Calculator()
    Res = Calc.Multiplication(1000, 0)
    assert Res == 0