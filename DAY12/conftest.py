#Fixtures written in this files are available across all testcase files
import pytest

from CalculatorAPI.Calculator import Calculator


@pytest.fixture()
def CreateCalculatorClassObj():
    C=Calculator()
    return C
    #If we want to return anything to the testcase method from fixture
    #we can do that with the help of return


@pytest.fixture()
def PositiveNumbers():
    print("Sending Positive Numbers")
    #Returning list of numbers
    return [1000,200,50]

#If we want to send parameterized data i.e.tuple having data with each interation
#We have to use params parametername to which List of tuple should be assigned
#
@pytest.fixture(params=[(3000,1000,2000), (5000,1000,4000),(100000,10000,90000)])
def SendPositiveNumbers(request):
    #return request.param returns single tuple from params with each iteration
    return request.param

#We can mention list of Dictionary objects instead of tuples in params
@pytest.fixture(params=[{"Num1":3000,"Num2":1000,"Result":2000}, {"Num1":5000,"Num2":1000,"Result":4000}])
def SendPositiveNumbersInDctionary(request):
    return request.param

