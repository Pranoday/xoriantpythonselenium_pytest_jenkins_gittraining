import pytest

from CalculatorAPI.Calculator import Calculator

#If we want to run all testcases from particular testcase file
#Command:pytest <path of the Testcase file>

#If we want to run particular testcase function
#Then command:pytest -k <Word which is present in the name of testcase function we want to run>
#@pytest.mar.skip is for Skipping the testcase altogether from execution
@pytest.mark.SanityTest
@pytest.mark.skip
def testMultiplicationWithPositiveNumbers():
    Calc = Calculator()
    Res = Calc.Multiplication(10,10)
    assert Res==100
#If we have any teestcases depending on particular testcase method which has
#a failed checkpoint.
#Now we can not skip this testcasde all together because of the dependency on this
#We can mark this testcase using @pytest.mark.xfail which executes the testcase
#but does not report its SUCCESS or FAILURE in final report
@pytest.mark.xfail
def testMultiplicationWithZero():
    Calc = Calculator()
    Res = Calc.Multiplication(1000, 0)
    assert Res == 0
