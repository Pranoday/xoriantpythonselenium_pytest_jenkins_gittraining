#If we define any fixture inside a testcase file,that is availlable only
#to the testcase functions from that file.
#But if we want to create a fixture which would be available across all
#testcase files,then we have to define such fixture in file called conftest.py
from CalculatorAPI.Calculator import Calculator


def testSubtractionWithPositiveNumbers(CreateCalculatorClassObj):
    #We make use of FixtureName in the parameter section of testcase method
    #to associate a fixture with Testcase method
    #In this reference variable of FixtureName,value returned by fixture gets stored
    #We can return object as well from fixtures,Object will also get recieved in
    #Fixture reference variable and we can use this reference variabl to
    #access members from class object of which we have got returned from fixture

    #C=Calculator()
    #Res=C.Subtraction(CreateCalculatorClassObj,10)
    #assert Res==100
    Res=CreateCalculatorClassObj.Subtraction(100,20)
    assert Res==80

def testSubtractionWithBigNumbers(SendPositiveNumbers):
    #In Data-driven we will recieve 1 tuple with each iteration in
    #SendPositiveNumbers variable
    #We can access sent data from recieved tuple using tuple index
    Cal=Calculator()
    Res=Cal.Subtraction(SendPositiveNumbers[0],SendPositiveNumbers[1])
    assert Res==SendPositiveNumbers[2]

def testSubtractionUsingDictionary(SendPositiveNumbersInDctionary):
    #Here we will recieve the list of Dictionary objects,hence accessing values will be using
    #keys and not using index
    Cal = Calculator()
    Res = Cal.Subtraction(SendPositiveNumbersInDctionary["Num1"], SendPositiveNumbersInDctionary["Num2"])
    assert Res == SendPositiveNumbersInDctionary["Result"]