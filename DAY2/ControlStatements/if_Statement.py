#If Statement
a=10
b=20
#if(a>b){}else{}
if(a>b):
    print(str(a)+" is bigger than "+str(b))
else:
    print(str(b)+" is bigger than "+str(a))

#Else If

a=100
b=200
c=300

if(a>b):
    if(a>c):
        print(str(a)+" is Bigger")
        print("I am inside Inner if")
    print("I am inside outer if")
elif (b>c):
    print(str(b) + " is Bigger")
    print("I am inside elif")
else:
    print(str(c)+" is bigger")
    print("I am inside else")

print("I am outside IF BLOCKS")