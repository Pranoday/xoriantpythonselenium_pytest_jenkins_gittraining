myfamily = {
  "child1" : {
    "name" : "Emil",
    "year" : 2004
  },
  "child2" : {
    "name" : "Tobias",
    "year" : 2007
  },
  "child3" : {
    "name" : "Linus",
    "year" : 2011
  }
}

print(myfamily)
print(myfamily["child3"])
print(myfamily["child3"]["name"])

for OuterK in myfamily.keys():
    #print(OuterK)
    #print(myfamily[OuterK])
    for InnerK in myfamily[OuterK].keys():
        print(myfamily[OuterK] [InnerK])

Cars={"Name":["Celerio","I10","I20"]}
print(Cars["Name"][1])

List1=["Xoriant","India"]
List2=["INfy","India"]
List3=["CapG","India"]
Dict1={"CarName":List1 ,
     "makein": List2,
      "IsMakeinIndia":List3}
#Traversing List elements from Keys
for K in Dict1.keys():
    for i in range(len(Dict1[K])):
        print(Dict1[K][i])

print("++++++")
for K in Dict1.keys():
    print(Dict1.values())
    for L in Dict1[K]:
        print(L)
