#Discussing Update and Other operations
Car={"Name":"Celerio","Make":"MarutiSuzuki","IsMadeInIndia":True,"Price":500000.50}

Car["Name"]="Alto K10"
print(Car["Name"])

#We can add Item i.e. new Key:Value pair in existing dictionary
Car.update({"YearOfManufacturing":2020})
print(Car)

#Adding duplicate Key using Update operation,will update existing Key's value
Car.update({"Name":"Swift"})
print(Car)

#For removing Item from Dictionary,use Pop
Car.pop("Name")
print(Car)



#Remove Item based on Value
print("Dictionary after removing Item based on Values")
for K in Car.keys():
    if(Car[K]==2020):
        Car.pop(K)
        break;

print(Car)

#popitem() removes the item which is at the END
Car.popitem()
print(Car)



#Printing Key-Value pair using for loop
print("Printing Ky-value using For loop")
for K,V in Car.items():
    print(K,V)

#clear will clear all the items from Dictionary but will keep the variable
Car.clear()
print("Printing Car dictionary after clearing ",Car)

#del will remove the dictionary variable from memory
del Car
print(Car)
