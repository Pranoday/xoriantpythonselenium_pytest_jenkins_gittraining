#More advanced List operation
Lst=["Banana",100,True,10.0]
print(Lst)

#Append Operation
#append operation will add new element at the END
Lst.append("Mango")
print(Lst)

#Insert Operation

#Insert will insert new element at specific Index,All elements which are at the Righ hand side
#will shift there Indexes by 1
Lst.insert(1,"Cherry")
print(Lst)

#Update Operation
#Update operation changes the value of existing element present at given Index
Lst[2]="NewValue"
print(Lst)
#remove operation will remove the Item from List,and shifts the elements
#present on the Right hand side to the Removed Element
print(Lst.remove(True))
#It will print Lst after removing True element
print(Lst)

#Pop operation:
#Pop operation returns element popped.
# It removes the element from List from specificx Index
#remove requires us to send Item to be removed and pop requires us to send Index
#of element to be removed
#remove does not return the element to be removed,but pop operation does
print(Lst.pop(0))
print("Popped element will still be removed from  List")
print(Lst)

#clear will clear the contents of List but Lst variables will still be kept in memory
Lst.clear()
print("Following are the contents of List: ")
print(Lst)
Lst.append("Banana")
print(Lst)

#del will delete the Contents as well as List variable,you will no longer be able to use it
del Lst
#print(Lst)

#Lst2=Lst1 will assign the reference of List variable to Lst2
Lst1=[10,20,30,40]
Lst2=["Pranoday","Sumit"]
Lst2=Lst1
print(Lst1)
print(Lst2)

#Copy operation needs 1 list variable to get the contents of other List variable copied
Lst3=["Banana","Mango"]
#Contcatenating Lists is same as Appending 1 list into another
print("Contcatenating of list: ")
print(Lst1+Lst3)
print("Before Copying Lst3 contents are following: ")
print(Lst3)
Lst3=Lst1.copy()
print("After Copying Lst3 contents are following: ")
print(Lst3)

