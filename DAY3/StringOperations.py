#All list slicing can be performed on Strings as String is nothing but
#list of characters
Name="Pranoday"
print("Revesring String",Name[::-1])
print("Retrieving Substring:",Name[1:5])

print("Printing whole string starting from particular Index: ",Name[1:])
print("Printing whole string",Name[:])
print("Printing 1st 5 characters from string ",Name[:5])