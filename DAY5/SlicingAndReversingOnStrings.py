#Slicing of String
#String are nothing but List of characters,hence all slicing operation
#we can perform on list,can be performed on strings as well
Name="Pranoday"
#Printing 1st 4 characters from string
print("Printing 1st 4 characters from string ",Name[0:4])

#To print whole string we can mention [:].In this case omitting
#start will take 0 as default
#omitting stop will take len(<string>) as defult
#Step would be taken as 1 by default
print("Printing whole string ",Name[:])

#Printing characters from MIDDLE from string

print("Printing characters from Index 2 to Index 5",Name[2:6])

#Printing string characters from End
#P  r  a   n  o   d   a   y
#-8 -7 -6 -5  -4  -3  -2  -1
#Printing last 3 characters from string
print("Printing last 3 characters from string",Name[-3:])

#If in slicing we are mentioning Negative number in Stop and Negative number
#in start then step should also be Negative

print("Printing few characters from end ",Name[-3:-5:-1])
#If start is Negative i.e.e.g. -3 and stop is -1,in this case
#index -1 falls on Right hand side of index -3
#then reading should always be from LEFT to RIGHT hence,steps should be POSITIVE
print(Name[-3:-1:1])

print("It will not print anything ",Name[-3:-8:-1])

print("Print ",Name[-3:1:-1])
#If we mention start as 1 and stop as -5,then
#Here it will read from Index 1 Left-Right and will stop at
#1 index prior which is -6 .
#Name[1:-5] O/P :ra

#0  1  2   3  4   5   6   7
#P  r  a   n  o   d   a   y
#-8 -7 -6 -5  -4  -3  -2  -1

print(Name[1:-5])

#In following example Reading should happen from LEFT-RIGHT
#as it will start from Index:3 and will go till -2 i.e 1 prior to mentioned
#Now -2 is present at RIGHT HAND SIDE of -3 hence reading should be done
#using POSITIVE Step i.e. :Left-Right
print(Name[3:-1:1])
print("No ",Name[3:-7:1])
#Name[3]

print(Name[3::])

print(Name[2:])