#In class methods 1st parameter holds the reference of the object
#used to call the method
#We can mention any name to this parameter
from AccountSystemExceptions.InsufficientBalanceException import InsufficientBalanceException


class Account:
    #Class variable
    InterestRate=10.0
    def __init__(self,ano,bal):
        #Instance variables
        self.AccountNo=ano
        self.Balance=bal

    def DepositeAmount(self,AmntToBeDeposited):
        self.Balance=self.Balance+AmntToBeDeposited

    def WithdrawAmount(self,AmountToBeWithdrawn):
        if self.Balance<AmountToBeWithdrawn:
            #raise is equivalent to throw in java
            raise InsufficientBalanceException()
        self.Balance=self.Balance-AmountToBeWithdrawn

    def GetBalance(self):
        return self.Balance