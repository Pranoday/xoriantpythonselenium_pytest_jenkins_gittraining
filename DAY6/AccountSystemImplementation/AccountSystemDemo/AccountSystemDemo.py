#In java we have instance variables and stativ variables
#In Python we have instance variables and class variables

#In java instance as well as static variables are defined inside class body
#where static variables are defined using static keword

#In python variables defined inside class are class variables
#variables defined using self,inside constructor or methods,are instance vars

#In java constructors hold same name as that of class
#In python contructor is defined using __init__(self)

#self from python is equivalent to this in java

from AccountSystem.AccountClass import Account
from AccountSystemExceptions.InsufficientBalanceException import InsufficientBalanceException

Act1= Account(1,100.00)
Act1.DepositeAmount(1000.00)

print("Current balance after deposite: ",Act1.GetBalance())

try:
    Act1.WithdrawAmount(2000.00)
    print("Current balance after withdrawal: ",Act1.GetBalance())
#except is equivalent to catch in java
except InsufficientBalanceException:
    print("You do not have sufficient balance in your account")

#Class variable can be accessed using object/class reference
print("Interest rate is ",Account.InterestRate)