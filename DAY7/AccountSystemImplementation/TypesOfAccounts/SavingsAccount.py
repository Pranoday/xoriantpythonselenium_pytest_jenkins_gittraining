#In java we use class SavingsAccount extends Account for inheritance
#In python:we have to mention Parent class name in paranthesis mentioned after child class name
from AccountSystem.AccountClass import Account


class SavingsAccount(Account):
    def __init__(self,*SavingsAccountDetails):
        #In java we have super keyword to call parent class constructor
        #In python we have super() function i.e.super().__init__()
        print("Hello")
        #Account.__init__(1)
        #Account.__init__(*SavingsAccountDetails)
        super().__init__(*SavingsAccountDetails)

    def WithdrawAmount(self,AmountToBeWithdrawn):

        print("Doing method overriding")
        RemainingBalance=self._Balance-AmountToBeWithdrawn
        if(self._Balance>AmountToBeWithdrawn and RemainingBalance>=1000):
            self._Balance=self._Balance-AmountToBeWithdrawn
        else:
            print("Withdrawal can not be processed as after withdrawal account balance is getting reduced than Rs.1000")

    def CloseAccount(self):
        super().CloseAccount()

