#If we want to import a particular Function/class/variable from a module
#exists in package then we have to use:
#from <PackageName>.<ModuleName> import <MemberName>

from Pkg1.File1 import Display
#from Pkg1.File1 import Display
from Pkg1 import  Display
from Pkg1.File1 import Display1
Display()
Display1()