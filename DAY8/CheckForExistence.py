from selenium import webdriver
import time
#In java:
#System.setProperty("webdriver.chrome.driver","PATH of Driver")
#ChromeDriver driver=new ChromeDriver()
from selenium.common.exceptions import NoSuchElementException

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

HideButton=driver.find_element_by_id("hide-textbox")
#We are clicking on button using click API
HideButton.click()
time.sleep(5)
#Retrieving list of elements having ID attribute set to displayed-text
Elements=driver.find_elements_by_id("displayed-text")
#Checking and confirming length of list is indeed 0
if(len(Elements)==0):
    print("Textbox got removed from DOM..Hide button works fine..PASSED")
else:
    print("Textbox got removed from DOM..Hide button does not work fine..FAILED")

#Identifying Show button
ShowButton=driver.find_element_by_id("show-textbox")
#Click on show button
ShowButton.click()
time.sleep(5)

#Retrieving list again of elements having ID attribute set to displayed-text
Elements=driver.find_elements_by_id("displayed-text")

if(len(Elements)==1):
    print("Textbox got added back to DOM..Show button works fine..PASSED")
else:
    print("Textbox got added back to DOM..Show button does not work fine..FAILED")

#2nd Way
HideButton=driver.find_element_by_id("hide-textbox")
#We are clicking on button using click API
HideButton.click()
time.sleep(5)
IsElementExists=0
try:
    #Here find_element_by_id should cause exception:NoSuchElementException
    #except block should get executed and IsElementExists variable should get set to 1
    driver.find_element_by_id("displayed-text")
except NoSuchElementException:
    IsElementExists=1
finally:
    print("It will get executed ALWAYS")
#Here confirming that IsElementExists variable has value 1
if(IsElementExists==1):
    print("Hide button works fine..PASSED")
else:
    print("Hide button does not work  fine..FAILED")

IsElementExists=0
#Click on show button
ShowButton.click()
time.sleep(5)
try:
    #Here element should be found and find_element_by_id should not cause exception
    #and except block should not be executed
    #IsElementPresent variable should not be set to 1
    driver.find_element_by_id("displayed-text")
except NoSuchElementException:
    IsElementExists=1

if(IsElementExists==0):
    print("Show button works fine..PASSED")
else:
    print("Show button does not work  fine..FAILED")