from selenium import webdriver
import time
driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

#When we do findElement with the reference of driver,element gets searched for
#in the entire DOM
#CheckboxHonda=driver.find_element_by_css_selector("input[value='honda']")
#CheckboxHonda.click()

#When we do findElement with the reference of any WebElement,
#then required element gets searched for inside CHILD Hierarchy of referenced element
DivElement=driver.find_element_by_id("checkbox-example")
DivElement.find_element_by_css_selector("input[value='honda']").click()