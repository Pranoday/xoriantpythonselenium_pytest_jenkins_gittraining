from selenium import webdriver
import time
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
driver.get("https://letskodeit.teachable.com/p/practice")

#<select id="carselect" name="cars">
#				<option value="bmw">BMW</option>  //0
#				<option value="benz">Benz</option>  //1
#				<option value="honda">Honda</option> //2
#</select>


#Identifying Dropdown using Id attribute and getting the scripting reference
CarsDropdown=driver.find_element_by_id("carselect")
#Sending the reference of UI element while creating object of Select class
#so that whenever we will now use any API to perfporm required operations
#operation will be performed on CORRECT UI element
Se=Select(CarsDropdown)

#In java we have getFirstSelectedOption()
#This returns us option tag as a WebElement corresponsing to Selected option
#SelectedOption=<option value="bmw">BMW</option>
SelectedOption=Se.first_selected_option
if SelectedOption.text=="BMW":
    print("Correct default option is selected...PASSED")
else:
    print("Wrong default option is selected...FAILED")
Se.select_by_index(2)
time.sleep(3)
Se.select_by_value("benz")
time.sleep(3)
Se.select_by_visible_text("BMW")

ExpectedCarNames=["BMW","Benz","Honda"]

AllOptions=Se.options
#AllOptions=[<option value="bmw">BMW</option>,<option value="benz">Benz</option>,<option value="honda">Honda</option>]
for Option in AllOptions:
    if(Option.text in ExpectedCarNames):
        print("Correct dropdown options are available..PASSED")
    else:
        print("Correct dropdown options are NOT available..FAILED")
        break

